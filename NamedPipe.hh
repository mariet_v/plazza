//
// NamedPipe.hh for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Tue Apr 22 16:00:54 2014 Mariette Valentin
// Last update Wed Apr 23 15:29:02 2014 Mariette Valentin
//

#ifndef NAMEDPIPE_HH_
# define NAMEDPIPE_HH_

#include <string>
#include <fstream>
#include <vector>
#include "PlazzaException.hh"

#define READ_SIZE (((512 * 12 + 8) / 2 - 1080 * 2 - 512) / 2 - 201)

class NamedPipe
{
public:
  NamedPipe(const std::string &);
  NamedPipe(int);
  ~NamedPipe();
  std::string get(void);
  void put(const std::string &);
  int getFd(void) const;

  NamedPipe &operator<<(const std::string &trame);
  NamedPipe &operator>>(std::string &trame);

private:
  int			_fd;
  std::string		_name;

};

#endif
