//
// Kitchen.cpp for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Tue Apr 22 15:17:28 2014 Mariette Valentin
// Last update Sun Apr 27 18:13:39 2014 Mariette Valentin
//
#include <sstream>
#include <unistd.h>
#include "Kitchen.hh"

void *myfunc(void *data)
{
  Cook *c = reinterpret_cast<Cook *> (data);
  c->work();
 return (NULL);
}

Kitchen::Kitchen(NamedPipe &in, NamedPipe &out, double cooking_time, int cooks_per_kitchen, int replacing_time) : _in(in), _out(out)
{
  int	i = 0;
  void	*foo;
  Cook	*ptr;

  _time = time(NULL);
  _cooking_time = cooking_time;
  _cooks_per_kitchen = cooks_per_kitchen;
  _replacing_time = replacing_time;
  _wait_list_pizza = 0;
   while (i < _cooks_per_kitchen)
    {
      ptr = new Cook(this);
      _cooks.push_back(ptr);
      foo = reinterpret_cast<void *> (ptr);
      _threads.push_back(new Thread(0, myfunc, foo));
      i++;
    }
   sendMsg("Kitchen created.");
}

Kitchen::~Kitchen()
{
}

void Kitchen::work(void)
{
  std::string pizza;
  int	i = 0;
  bool	done = false;

  while (time(NULL) - _time < 5 || _wait_list_pizza > 0)
    {
      pizza = getMsg();
      if (pizza.compare(""))
	{
	  _time = time(NULL);
	  done = false;
	  while (i < _cooks_per_kitchen && !done)
	    {
	      if (!_cooks[i]->isBusy())
		{
		  _cooks[0]->assign_pizza(pizza);
		  _wait_list_pizza++;
		  pizza = "";
		  done = true;
		}
	      i++;
	    }
	}
      usleep(10000);
    }
  i = 0;
  while (i < _cooks_per_kitchen)
    _cooks[i++]->haveToStop();
  sendMsg("Kitchen stoped.");
}

void Kitchen::sendMsg(const std::string &msg)
{
  _out << msg;
}

std::string Kitchen::getMsg(void)
{
  std::string msg;
  _in >> msg;
  return (msg);
}

void Kitchen::received_pizza(std::string &pizza)
{
  std::stringstream ss;

  ss << "Finished pizza : " << pizza << std::endl;
  _wait_list_pizza--;
  sendMsg(ss.str());
}

double Kitchen::getCookingTime(void) const
{
  return (_cooking_time);
}
