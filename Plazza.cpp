//
// Plazza.cpp for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Thu Apr 17 15:54:38 2014 Mariette Valentin
// Last update Sun Apr 27 19:15:11 2014 Mariette Valentin
//
#include <sstream>
#include <stdlib.h>
#include <unistd.h>
#include "Plazza.hh"
#include "Kitchen.hh"

Plazza::Plazza(int argc, char **argv) : _logs("PlazzaLogs", std::ios::out | std::ios::app)
{
  std::stringstream ss;

  if (argc < 4)
    throw PlazzaException("Parameters not valid");
  ss << argv[1] << " " << argv[2] << " " << argv[3];
  ss >> _cooking_time >> _cooks_per_kitchen >> _replacing_time;
 std::cout << _cooking_time << _cooks_per_kitchen << _replacing_time << std::endl;
  _pipe_number = 1;
}

Plazza::~Plazza()
{
  _logs.close();
}

double Plazza::getCookingTime(void) const
{
  return (_cooking_time);
}

int Plazza::getCooksNumber(void) const
{
  return (_cooks_per_kitchen);
}

int Plazza::getReplacingTime(void) const
{
  return (_replacing_time);
}

void Plazza::work(void)
{
  std::string str;

  while (getline(std::cin, str))
    {
      if (!execCmd(str))
	return;
      checkKitchenRespond();
    }
}

int Plazza::execCmd(std::string &str)
{
  std::string pizza;
  std::string number;
  std::stringstream ss;
  unsigned int	num = 1;
  unsigned int	i = 0;
  int	nbr = 0;

  pizza = str.substr(0, str.find(' '));
  if (!str.compare("exit"))
    return (0);  
  else if (!pizza.compare("regina") || !pizza.compare("margarita") || !pizza.compare("americana") || !pizza.compare("fantasia"))
    {
      while (str[str.size() - 1] == ' ')
	str.erase(str.size() - 1, 1);
      if (str[str.size() - 1] == ';')
	str.erase(str.size() - 1, 1);
      i = 0;
      while (i < str.size() - 1)
	{
	  if (str[i] == ' ' && str[i + 1] == ' ')
	    str.erase(i, 1);
	  else
	    i++;
	}
      i = 0;
      while (i < str.size())
	{
	  if (str[i] == ' ')
	    nbr++;
	  i++;
	}
      if (nbr != 2)
	return (1);
      std::cout << str << std::endl;
      number = str.substr(str.rfind(' '), str.size() - 1);
      number = number.substr(number.find('x') + 1 , number.size() - 1);
      std::cout << number << std::endl;
      ss << number;
      ss >> num;
      i = 0;
      while (i < num)
	{
	  pizza = str.substr(0, str.rfind(' '));
	  addPizza(pizza);
	  i++;
	}
    }
  return (1);
}

void Plazza::addKitchen(void)
{
  std::stringstream ss;
  std::stringstream ss2;

  ss << "tokitchen" << _pipe_number;
  ss2 << "toentry" << _pipe_number;
  _pipe_number++;
  NamedPipe entryToKitchen(ss.str());
  NamedPipe kitchenToEntry(ss2.str());
  pid_t pid = fork();
  Kitchen* newkitchen;

  switch (pid)
    {
    case -1:
      throw PlazzaException("Cannot create kitchen : fork fail !");
      break;
      
    case 0:
      newkitchen = new Kitchen(entryToKitchen, kitchenToEntry, _cooking_time, _cooks_per_kitchen, _replacing_time);
      newkitchen->work();
      exit(1);
      //fils
      break;

    default:
      _kitchens.push_back(KitchenEntry(kitchenToEntry, entryToKitchen, _cooking_time, _cooks_per_kitchen, _replacing_time));
     //pere
      break;
    }
}

void Plazza::addPizza(std::string &pizza)
{
 unsigned int	i = 0;

  while (i < _kitchens.size() && _kitchens[i].isBusy())
    i++;
  if (i < _kitchens.size())
    _kitchens[i].addPizza(pizza);
  else
    {
      addKitchen();
      _kitchens[i].addPizza(pizza);
    }
}

void Plazza::checkKitchenRespond(void)
{
  unsigned int i = 0;
  std::string msg;

  while (i < _kitchens.size())
    {
      msg = _kitchens[i].getMsg();
      if (msg.compare(""))
	{
	  if (!(msg.substr(0, 17)).compare("Finishing pizza"))
	    _kitchens[i].finishingPizza();
	  if (!msg.compare("Kitchen stoped."))
	    _kitchens.erase(_kitchens.begin() + i);
	  std::cout << msg << std::endl;
	  _logs << msg << std::endl;
	}
      i++;
    }
}
