//
// KitchenEntry.hh for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Thu Apr 17 16:47:06 2014 Mariette Valentin
// Last update Sun Apr 27 16:43:03 2014 Mariette Valentin
//

#ifndef KITCHENENTRY_HH_
#define KITCHENENTRY_HH_
#include <iostream>
#include <sstream>
#include "NamedPipe.hh"

class KitchenEntry
{
public:
  KitchenEntry(NamedPipe &in, NamedPipe &out, double cooking_time, int cooks_per_kitchen, int replacing_time);
  ~KitchenEntry();
  bool	isBusy(void) const;
  void	sendMsg(const std::string &msg);
  std::string getMsg(void);
  void	addPizza(std::string &pizza);
  void	finishingPizza(void);

private:
  NamedPipe		_in;
  NamedPipe		_out;
  int		_kitchen_pid;
  double	_cooking_time;
  int		_cooks_per_kitchen;
  int		_replacing_time;
  int		_wait_list_pizza;
};

#endif
