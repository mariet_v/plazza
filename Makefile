##
## Makefile for makefile in /home/mariet_v/piscine/day06/ex00
## 
## Made by Mariette Valentin
## Login   <mariet_v@epitech.net>
## 
## Started on  Mon Jan 13 09:15:10 2014 Mariette Valentin
## Last update Sat Apr 26 15:58:52 2014 Mariette Valentin
##

CC	= g++

CPPFLAGS	= -W -Wall

LIBS	= -lpthread

SRC	= main.cpp \
	Plazza.cpp \
	PlazzaException.cpp \
	KitchenEntry.cpp \
	Kitchen.cpp \
	NamedPipe.cpp \
	Cook.cpp \
	Thread.cpp

OBJ	= $(SRC:.cpp=.o)

NAME	= plazza

$(NAME):  $(OBJ)
	g++ $(CPPFLAGS) $(LIBS) $(OBJ) -o $(NAME)

all:    $(NAME)

clean:
	rm -f $(OBJ)

fclean:  clean
	rm -rf $(NAME)
re : fclean $(NAME)

reclean: re
	rm -f $(OBJ)
