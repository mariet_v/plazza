//
// plazza.hh for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Thu Apr 17 15:52:37 2014 Mariette Valentin
// Last update Sun Apr 27 16:10:32 2014 Mariette Valentin
//

#ifndef PLAZZA_HH_
# define PLAZZA_HH_
#include <iostream>
#include <fstream>
#include <vector>
#include "PlazzaException.hh"
#include "KitchenEntry.hh"

class Plazza
{
public:
  Plazza(int argc, char **argv);
  ~Plazza();
  void	work(void);
  int	execCmd(std::string& str);
  void	addKitchen(void);
  void	addPizza(std::string &pizza);
  void	checkKitchenRespond(void);
  double	getCookingTime(void) const;
  int		getCooksNumber(void) const;
  int		getReplacingTime(void) const;

  private:
  double       	_cooking_time;
  int		_cooks_per_kitchen;
  int		_replacing_time;
  std::vector<KitchenEntry>	_kitchens;
  int		_pipe_number;
  std::ofstream _logs;
};

#endif
