//
// PlazzaException.hh for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Thu Apr 17 16:11:51 2014 Mariette Valentin
// Last update Thu Apr 17 16:29:33 2014 Mariette Valentin
//

#ifndef PLAZZAEXCEPTION_HH_
# define PLAZZAEXCEPTION_HH_
#include <exception>
#include <string>

class PlazzaException : public std::exception
{
public:
  PlazzaException(std::string const & error) throw();
  ~PlazzaException() throw();
  virtual const char* what() const throw();
  
private:
  std::string	_error;
};

#endif
