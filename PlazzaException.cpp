//
// PlazzaException.cpp for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Thu Apr 17 16:14:37 2014 Mariette Valentin
// Last update Thu Apr 17 16:30:45 2014 Mariette Valentin
//

#include "PlazzaException.hh"

PlazzaException::PlazzaException(std::string const & error) throw()
{
  _error = error;
}

PlazzaException::~PlazzaException() throw()
{

}

const char* PlazzaException::what() const throw()
{
  return (_error.c_str());
}

