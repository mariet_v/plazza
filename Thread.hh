//
// Thread.hh for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Sat Apr 26 15:46:31 2014 Mariette Valentin
// Last update Sat Apr 26 16:38:59 2014 Mariette Valentin
//

#ifndef THREAD_HH_
#define THREAD_HH_
#include <pthread.h>

class Thread
{
public:
  Thread(pthread_attr_t *attr, void *(*func)(void *), void *arg);
  ~Thread();
  void exit(void);
  int join(void);
  int detach(void);
  const pthread_t *getThread(void) const;

private:
  pthread_t	_thread;
};

#endif
