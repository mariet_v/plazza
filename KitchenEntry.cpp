//
// KitchenEntry.cpp for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Thu Apr 17 16:46:53 2014 Mariette Valentin
// Last update Sun Apr 27 18:53:46 2014 Mariette Valentin
//

#include "KitchenEntry.hh"

KitchenEntry::KitchenEntry(NamedPipe &in, NamedPipe &out, double cooking_time, int cooks_per_kitchen, int replacing_time) : _in(in), _out(out) 
{
  _cooking_time = cooking_time;
  _cooks_per_kitchen = cooks_per_kitchen;
  _replacing_time = replacing_time;
  _wait_list_pizza = 0;
}

KitchenEntry::~KitchenEntry()
{
 
}

bool KitchenEntry::isBusy(void) const
{
  if (_wait_list_pizza < _cooks_per_kitchen)
    return (false);
  else 
    return (true);
}

void KitchenEntry::sendMsg(const std::string &msg)
{
  _out << msg;
}

std::string KitchenEntry::getMsg(void)
{
  std::string msg;
  _in >> msg;
  return (msg);
}

void KitchenEntry::addPizza(std::string &pizza)
{
  std::cout << "Transfert aux cuisines d'une pizza " << pizza << std::endl;
  sendMsg(pizza);
  _wait_list_pizza++;
}

void KitchenEntry::finishingPizza(void)
{
  _wait_list_pizza--;
}
