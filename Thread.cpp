//
// Thread.cpp for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Sat Apr 26 15:46:21 2014 Mariette Valentin
// Last update Sat Apr 26 17:12:02 2014 Mariette Valentin
//
#include <iostream>
#include <pthread.h>
#include "Thread.hh"

Thread::Thread(pthread_attr_t *attr, void *(*func)(void *), void *arg)
{
  pthread_create(&(_thread), attr, func, arg);
}

Thread::~Thread()
{
  std::cout << "foooooo" << std::endl;
  this->detach();
  this->exit();
}

void Thread::exit(void)
{
  int	return_value;

  return_value = 0;
  pthread_exit((void *) &return_value);
}

int Thread::join(void)
{
  return (pthread_join(_thread, NULL));
}

int Thread::detach(void)
{
  std::cout << " ce thread est detach" << std::endl;
  return (pthread_detach(_thread));
}

const pthread_t *Thread::getThread(void) const
{
  return &(_thread);
}
