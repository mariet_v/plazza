//
// Cooks.hh for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Sat Apr 26 15:29:19 2014 Mariette Valentin
// Last update Sun Apr 27 18:02:39 2014 Mariette Valentin
//

#ifndef COOK_HH_
#define COOK_HH_
#include <iostream>
#include "Kitchen.hh"

class Kitchen;

class Cook
{
public:
  Cook(Kitchen *kitchen);
  ~Cook();
  void	assign_pizza(std::string &pizza);
  void	work(void);
  void	haveToStop(void);
  int	getPizzaCookingTime(std::string &pizza) const;
  bool	isBusy(void) const;

private:
  Kitchen	*_kitchen;
  std::string	_pizza;
  std::string	_next_pizza;
  bool		_have_to_stop;
  bool		_is_busy;
};
#endif
