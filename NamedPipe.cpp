//
// NamedPipe.cpp for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Tue Apr 22 16:03:19 2014 Mariette Valentin
// Last update Sun Apr 27 18:54:12 2014 Mariette Valentin
//

#include <sys/select.h>
#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <string.h>
#include <errno.h>

#include "NamedPipe.hh"

NamedPipe::NamedPipe(const std::string &name)
{
  _name = name;
  mkfifo(name.c_str(), S_IRWXU);
  this->_fd = open(name.c_str(), O_RDWR | O_CREAT | O_NONBLOCK);
  if (this->_fd < 0)
      throw PlazzaException("Named pipe error");
}

NamedPipe::NamedPipe(int fd)
{
  _fd = fd;
}

NamedPipe::~NamedPipe()
{
}


std::string NamedPipe::get()
{
  char buff[READ_SIZE + 1];
  int size;
  bool done = false;
  std::string line("");

  while (!done && (size = read(this->_fd, buff, READ_SIZE)) > 0)
    {
      if (buff[0] == '\n')
	done = true;
      else
	line.push_back(buff[0]);
    }
  return (line);
}

void NamedPipe::put(const std::string &msg)
{
  const char *buff;

  buff = msg.c_str();
  write(this->_fd, buff, strlen(buff));
  write(this->_fd, "\n", 1);
}

int NamedPipe::getFd(void) const
{
  return (this->_fd);
}

NamedPipe& NamedPipe::operator<<(const std::string &trame)
{
  this->put(trame);
  return (*this);
}

NamedPipe& NamedPipe::operator>>(std::string &trame)
{
  trame = this->get();
  return (*this);
}
