//
// main.cpp for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Thu Apr 17 15:51:58 2014 Mariette Valentin
// Last update Thu Apr 17 16:31:00 2014 Mariette Valentin
//
#include "PlazzaException.hh"
#include "Plazza.hh"

int main(int argc, char **argv)
{
  Plazza plazza(argc, argv);

  try 
    {
      plazza.work();
    }
  catch (std::exception& e)
    {
      std::cerr << "exception caugth : " << e.what() << std::endl;
    }
}
