//
// Kitchen.hh for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Tue Apr 22 15:15:59 2014 Mariette Valentin
// Last update Sun Apr 27 17:01:25 2014 Mariette Valentin
//

#ifndef KITCHEN_HH_
# define KITCHEN_HH_
#include <iostream>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include "NamedPipe.hh"
#include "Cook.hh"
#include "Thread.hh"

class Cook;

class Kitchen
{
public:
  Kitchen(NamedPipe &in, NamedPipe &out, double cooking_time, int cooks_per_kitchen, int replacing_time);
  ~Kitchen();
  void work(void);
  void sendMsg(const std::string &msg);
  std::string getMsg(void);
  void	received_pizza(std::string &pizza);
  double getCookingTime(void) const;

private:
  NamedPipe		_in;
  NamedPipe		_out;
  time_t		_time;
  double		_cooking_time;
  int			_cooks_per_kitchen;
  int			_replacing_time;
  std::vector<Cook *>	_cooks;
  std::vector<Thread *>	_threads;
  int			_wait_list_pizza;
};
#endif
