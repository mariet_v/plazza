//
// Cook.cpp for plazza in /home/mariet_v/celem/plazza
// 
// Made by Mariette Valentin
// Login   <mariet_v@epitech.net>
// 
// Started on  Sat Apr 26 15:30:06 2014 Mariette Valentin
// Last update Sun Apr 27 18:13:01 2014 Mariette Valentin
//
#include <unistd.h>
#include "Cook.hh"

Cook::Cook(Kitchen *kitchen)
{
  _have_to_stop = false;
  _is_busy = false;
  _pizza = "";
  _kitchen = kitchen;
}

Cook::~Cook()
{
}

void Cook::assign_pizza(std::string &pizza)
{
  _pizza = pizza;
  _is_busy = true;
}

void Cook::work(void)
{
  std::string pizza_name;

  while (!_have_to_stop)
    {
      if (_pizza.compare(""))
	{
	  pizza_name = _pizza.substr(0, _pizza.find(' '));
	  usleep(getPizzaCookingTime(pizza_name) * _kitchen->getCookingTime() * 1000000);
	  _kitchen->received_pizza(_pizza);
	  _is_busy = false;
	  _pizza = "";
	}
       usleep(100000);
    }
}

void Cook::haveToStop(void)
{
  _have_to_stop = true;
}

int Cook::getPizzaCookingTime(std::string &pizza) const
{
  if (!pizza.compare("margarita"))
    return (1);
  else if (!pizza.compare("regina") || !pizza.compare("americana"))
    return (2);
  else if (!pizza.compare("fantasia"))
    return (4);
  else
    return (0);
}

bool Cook::isBusy(void) const
{
  return (_is_busy);
}
